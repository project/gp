<?php

/**
 * Implements hook_requirements().
 */
function gp_requirements($phase) {
  module_load_include('inc', 'gp', 'gp.helpers');

  $requirements = array();
  $t = get_t();

  if ($phase == 'install' || $phase == 'runtime') {
    if (!gp_load_places(FALSE)) {
      $requirements['gp'] = array(
        'title' => $t('Google Places'),
        'severity' => REQUIREMENT_ERROR,
        'description' => $t('The Google Places module requires the <strong>google-places</strong> library, which is missing. See the README.txt file for installation instructions.'),
      );
    }
  }

  if ($phase == 'runtime' && !isset($requirements['gp'])) {
    if (!gp_api_key()) {
      $requirements['gp'] = array(
        'title' => $t('Google Places'),
        'severity' => REQUIREMENT_ERROR,
        'description' => $t('The Google Places API Key is missing. Visit the <a href="@url">configuration page</a> to enter your key.',
          array('@url' => url('admin/config/services/gp'))),
      );
    }
  }

  return $requirements;
}

/**
 * Implements hook_uninstall().
 */
function gp_uninstall() {
  variable_del('gp_api_key');
  variable_del('gp_cache_expiration');
  variable_del('gp_cache_queue_worker_time');
}

/**
 * Implements hook_schema().
 */
function gp_schema() {
  $schema['cache_gp'] = drupal_get_schema_unprocessed('system', 'cache');
  return $schema;
}
