<?php

/**
 * @file Google Places helper functions.
 */

/**
 * Return the API key.
 */
function gp_api_key() {
  return variable_get('gp_api_key', NULL);
}

/**
 * Return the cache expiration time.
 */
function gp_cache_expiration() {
  return variable_get('gp_cache_expiration', 0);
}

/**
 * Return the queue worker time.
 */
function gp_cache_queue_worker_time() {
  return variable_get('gp_cache_queue_worker_time', 5);
}

/**
 * Load the googlePlaces library.
 */
function gp_load_places($check_api_key = TRUE) {
  static $googlePlaces;
  if (!isset($googlePlaces)) {
    if ($check_api_key) {
      // Check the key.
      $key = gp_api_key();
      if (empty($key)) {
        watchdog('gp', 'The Google Places API Key is missing. Visit the <a href="@url">configuration page</a> to enter your key.',
          array('@url' => url('admin/config/services/gp')), WATCHDOG_ERROR
        );
        return;
      }
    }

    $lib_path = function_exists('libraries_get_path') ? libraries_get_path('google-places') : 'sites/all/libraries/google-places';
    if (!is_dir($lib_path)) {
      // Deprecated path.
      $lib_path = function_exists('libraries_get_path') ? libraries_get_path('google_places_php') : 'sites/all/libraries/google_places_php';
    }
    $class_path = $lib_path . '/src/mills/google-places/googlePlaces.php';

    if (file_exists($class_path)) {
      include_once($class_path);
      if (class_exists('Mills\\GooglePlaces\\googlePlaces')) {
        $key = gp_api_key();
        $googlePlaces = new Mills\GooglePlaces\googlePlaces($key);
      }
      else {
        watchdog('gp', 'Unable to load the google-places library. Class doesn\'t exist. See the README.txt file for installation instructions.', array(), WATCHDOG_ERROR);
      }
    }
    else {
      watchdog('gp', 'The Google Places module requires the <strong>google-places</strong> library, which is missing. See the README.txt file for installation instructions.', array(), WATCHDOG_ERROR);
    }
  }
  return $googlePlaces;
}

/**
 * Format address components that were returned from a googlePlaces lookup.
 *
 * @return An address in the format of the addressfield module.
 */
function gp_address_from_components($components) {
  $address = array(
    'street_number' => '',
    'route' => '',
    'thoroughfare' => '',
    'premise' => '',
    'locality' => '',
    'postal_town' => '',
    'administrative_area' => '',
    'postal_code' => '',
    'country' => '',
  );

  foreach ($components as $component) {
    $component = (array)$component;
    foreach ($component['types'] as $type) {
      switch ($type) {
        case 'administrative_area_level_1':
          $address[$type] = drupal_strtoupper(trim($component['short_name'], '.'));
          break;
        case 'country':
          $address[$type] = $component['short_name'];
          break;
        default:
          $address[$type] = $component['long_name'];
          break;
      }
    }
  }

  // Format and build out certain parts of the address.
  $address['thoroughfare'] = $address['street_number'] . ' ' . $address['route'];
  $address['administrative_area'] = $address['administrative_area_level_1'];

  // Special cases by country.
  switch ($address['country']) {
    case 'AT': // Austria
    case 'DE': // Germany
      $address['thoroughfare'] = $address['route'] . ' ' . $address['street_number'];
      break;
    case 'GB': // United Kingdom
      $address['locality'] = $address['postal_town'];
      break;
  }

  // Last bit of cleaning.
  $address['thoroughfare'] = str_replace('  ', ' ', trim($address['thoroughfare']));

  return $address;
}

