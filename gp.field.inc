<?php

/**
 * @file Google Places field functionality.
 */

/**
 * Implements hook_field_info().
 */
function gp_field_info() {
  return array(
    'google_place' => array(
      'label' => t('Google Place'),
      'description' => t('This field stores a Google Place ID and data about it.'),
      'default_widget' => 'google_place_field',
      'default_formatter' => 'google_place_default',
      'property_type' => 'text',
    ),
  );
}

/**
 * Implements hook_field_schema().
 */
function gp_field_schema($field) {
  return array(
    'columns' => array(
      'google_place_id' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
    ),
  );
}


/**
 * Implements hook_field_widget_info().
 */
function gp_field_widget_info() {
  return array(
    'google_place_field' => array(
      'label' => t('Google Place'),
      'field types' => array('google_place'),
    ),
  );
}

/**
 * Implements hook_field_settings_form().
 * @todo Is this needed?
 */
function gp_field_settings_form($field, $instance, $has_data) {
  $settings = $field['settings'];
  // Add your global settings fields here
  $form = array();
  return $form;
}

/**
 * Implements hook_field_instance_settings_form().
 */
function gp_field_instance_settings_form($field, $instance) {

  $settings = $instance['settings'];

  $form = array();

  // Give the user the option of saving the place name as title/label.
  $form['set_label'] = array(
    '#type' => 'checkbox',
    '#title' => t('Set the title/label to the Google Place name'),
    '#description' => t('Checking this option will disable the title/label field on the edit form.'),
    '#default_value' => isset($instance['settings']['set_label']) ? $instance['settings']['set_label'] : TRUE,
  );

  // Load the fields for this content type, and group them by data type.
  $fields = field_info_instances($instance['entity_type'], $instance['bundle']);
  $addressfields = array();
  $geofields = array();
  $telephone_fields = array();
  $url_fields = array();
  $taxonomy_fields = array();
  $integer_fields = array();
  $text_fields = array();
  $text_long_fields = array();
  foreach ($fields as $field_name => $field) {
    $field_info = field_info_field($field['field_name']);
    switch ($field_info['type']) {
      case 'addressfield': $addressfields[$field_name] = $field_info; break;
      case 'geofield': $geofields[$field_name] = $field_info; break;
      case 'telephone': $telephone_fields[$field_name] = $field_info; break;
      case 'url': $url_fields[$field_name] = $field_info; break;
      case 'taxonomy_term_reference': $taxonomy_fields[$field_name] = $field_info; break;
      case 'number_integer': $integer_fields[$field_name] = $field_info; break;
      case 'text': $text_fields[$field_name] = $field_info; break;
      case 'text_long': $text_long_fields[$field_name] = $field_info; break;
    }
  }

  /*
  // @todo Attach these messages to each form field below.

  $valid = TRUE;
  if (empty($addressfields)) {
    $valid = FALSE;
    drupal_set_message(t('You need to create an address field!'), 'warning');
  }
  if (empty($geofields)) {
    $valid = FALSE;
    drupal_set_message(t('You need to create a geo field!'), 'warning');
  }
  if (empty($telephone_fields)) {
    $valid = FALSE;
    drupal_set_message(t('You need to create a telephone field!'), 'warning');
  }
  if (empty($url_fields)) {
    $valid = FALSE;
    drupal_set_message(t('You need to create a url field!'), 'warning');
  }
  else if (sizeof($url_fields) < 3) {
    $valid = FALSE;
    drupal_set_message(t('You need to create at least 3 url fields!'), 'warning');
  }
  if (empty($taxonomy_fields)) {
    $valid = FALSE;
    drupal_set_message(t('You need to create a taxonomy term reference field!'), 'warning');
  }
  if (empty($integer_fields)) {
    $valid = FALSE;
    drupal_set_message(t('You need to create an integer field!'), 'warning');
  }
  if (empty($text_fields)) {
    $valid = FALSE;
    drupal_set_message(t('You need to create a text field!'), 'warning');
  }
  if (!$valid) {
    drupal_set_message(t('Come back to this form when you have created the required field(s) on this content type.'), 'error');
    return $form;
  }
  */

  // Build the select lists for each field so the user can specify which fields
  // to store the place data in.

  // Addressfield
  $options = array('' => t('- Select -'));
  foreach($addressfields as $field_name => $addressfield) {
    $options[$field_name] = $fields[$field_name]['label'];
  }
  $form['addressfield'] = array(
    '#type' => 'select',
    '#title' => t('Address'),
    '#description' => t('Select an address field to store cached data.'),
    '#required' => FALSE,
    '#options' => $options,
    '#default_value' => isset($instance['settings']['addressfield']) ? $instance['settings']['addressfield'] : ''
  );

  // Formatted Address
  $options = array('' => t('- Select -'));
  foreach($text_fields as $field_name => $text_field) {
    $options[$field_name] = $fields[$field_name]['label'];
  }
  $form['formatted_address'] = array(
    '#type' => 'select',
    '#title' => t('Formatted address'),
    '#description' => t('Select a text field to store cached data.'),
    '#required' => FALSE,
    '#options' => $options,
    '#default_value' => isset($instance['settings']['formatted_address']) ? $instance['settings']['formatted_address'] : ''
  );

  // Geofield
  $options = array('' => t('- Select -'));
  foreach($geofields as $field_name => $geofield) {
    $options[$field_name] = $fields[$field_name]['label'];
  }
  $form['geofield'] = array(
    '#type' => 'select',
    '#title' => t('Geofield'),
    '#description' => t('Select a geo field to store cached data.'),
    '#required' => FALSE,
    '#options' => $options,
    '#default_value' => isset($instance['settings']['geofield']) ? $instance['settings']['geofield'] : ''
  );

  // Telephone
  $options = array('' => t('- Select -'));
  foreach($telephone_fields as $field_name => $telephone_field) {
    $options[$field_name] = $fields[$field_name]['label'];
  }
  $form['telephone'] = array(
    '#type' => 'select',
    '#title' => t('Telephone'),
    '#description' => t('Select a telephone field to store cached data.'),
    '#required' => FALSE,
    '#options' => $options,
    '#default_value' => isset($instance['settings']['telephone']) ? $instance['settings']['telephone'] : ''
  );

  // Website
  $options = array('' => t('- Select -'));
  foreach($url_fields as $field_name => $url_field) {
    $options[$field_name] = $fields[$field_name]['label'];
  }
  $form['website'] = array(
    '#type' => 'select',
    '#title' => t('Website URL'),
    '#description' => t('Select a url field to store the website URL cached data.'),
    '#required' => FALSE,
    '#options' => $options,
    '#default_value' => isset($instance['settings']['website']) ? $instance['settings']['website'] : ''
  );

  // Icon
  $options = array('' => t('- Select -'));
  foreach($url_fields as $field_name => $url_field) {
    $options[$field_name] = $fields[$field_name]['label'];
  }
  $form['icon'] = array(
    '#type' => 'select',
    '#title' => t('Google Maps Icon URL'),
    '#description' => t('Select a url field to store the Google Maps icon URL cached data.'),
    '#required' => FALSE,
    '#options' => $options,
    '#default_value' => isset($instance['settings']['icon']) ? $instance['settings']['icon'] : ''
  );

  // URL (Google Plus)
  $options = array('' => t('- Select -'));
  foreach($url_fields as $field_name => $url_field) {
    $options[$field_name] = $fields[$field_name]['label'];
  }
  $form['url'] = array(
    '#type' => 'select',
    '#title' => t('Google Plus URL'),
    '#description' => t('Select a url field to store the Google Plus URL cached data.'),
    '#required' => FALSE,
    '#options' => $options,
    '#default_value' => isset($instance['settings']['url']) ? $instance['settings']['url'] : ''
  );

  // Place Types (via taxonomy terms)
  $options = array('' => t('- Select -'));
  foreach($taxonomy_fields as $field_name => $taxonomy_field) {
    $options[$field_name] = $fields[$field_name]['label'];
  }
  $form['types'] = array(
    '#type' => 'select',
    '#title' => t('Place types'),
    '#description' => t('Select a taxonomy term reference field to store cached data.'),
    '#required' => FALSE,
    '#options' => $options,
    '#default_value' => isset($instance['settings']['types']) ? $instance['settings']['types'] : ''
  );

  // Price Level
  $options = array('' => t('- Select -'));
  foreach($integer_fields as $field_name => $integer_field) {
    $options[$field_name] = $fields[$field_name]['label'];
  }
  $form['price_level'] = array(
    '#type' => 'select',
    '#title' => t('Price level'),
    '#description' => t('Select a integer field to store cached data.'),
    '#required' => FALSE,
    '#options' => $options,
    '#default_value' => isset($instance['settings']['price_level']) ? $instance['settings']['price_level'] : ''
  );

  // UTC Offset
  $options = array('' => t('- Select -'));
  foreach($integer_fields as $field_name => $integer_field) {
    $options[$field_name] = $fields[$field_name]['label'];
  }
  $form['utc_offset'] = array(
    '#type' => 'select',
    '#title' => t('UTC Offset'),
    '#description' => t('Select a integer field to store cached data.'),
    '#required' => FALSE,
    '#options' => $options,
    '#default_value' => isset($instance['settings']['utc_offset']) ? $instance['settings']['utc_offset'] : ''
  );

  // Weekday text
  $options = array('' => t('- Select -'));
  foreach($text_long_fields as $field_name => $text_field) {
    $options[$field_name] = $fields[$field_name]['label'];
  }
  $form['weekday_text'] = array(
    '#type' => 'select',
    '#title' => t('Weekday text'),
    '#description' => t('Select a text long field to store the cached data.'),
    '#required' => FALSE,
    '#options' => $options,
    '#default_value' => isset($instance['settings']['weekday_text']) ? $instance['settings']['weekday_text'] : ''
  );

  // Photo(s)
  $options = array('' => t('- Select -'));
  foreach($text_fields as $field_name => $text_field) {
    $options[$field_name] = $fields[$field_name]['label'];
  }
  $form['photos'] = array(
    '#type' => 'select',
    '#title' => t('Photos'),
    '#description' => t('Select a text field to store the photo(s) cached data.'),
    '#required' => FALSE,
    '#options' => $options,
    '#default_value' => isset($instance['settings']['photos']) ? $instance['settings']['photos'] : ''
  );

  return $form;
}

/**
 * Implements hook_field_widget_form().
 */
function gp_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  switch ($instance['widget']['type']) {
    case 'google_place_field' :
      // Disable the title field if the title/label is being set.
      if (isset($instance['settings']['set_label']) ? $instance['settings']['set_label'] : TRUE) {
        $form['title']['#required'] = FALSE;
        $form['title']['#disabled'] = TRUE;
        $form['title']['#description'] = t('This field is disabled because it is set from the <em>Google Place name</em>.');
      }

      $element['google_place_id'] = array(
        '#type' => 'textfield',
        '#title' => t('Google Place ID'),
        '#default_value' => isset($items[$delta]['google_place_id']) ? $items[$delta]['google_place_id'] : '',
        '#required' => $element['#required'],
        '#size' => 64,
      );
      break;
  }
  return $element;
}

/**
 * Implements hook_field_presave().
 */
function gp_field_presave($entity_type, $entity, $field, $instance, $langcode, &$items) {
  // Collect the new field values.
  $field_values = array();

  foreach ($items as $delta => $item) {
    if (isset($item['google_place_id'])) {
      $place = array();
      $place_id = $item['google_place_id'];

      $items[$delta]['google_place_id'] = $place_id;

      // Look up this item's place details, log any errors to watchdog.
      if ($cache = cache_get($place_id, 'cache_gp')) {
        $place = $cache->data;
      }
      else {
        // Retrieve the place details.
        $googlePlaces = gp_load_places();
        if (!$googlePlaces) {
          return;
        }
        $googlePlaces->setPlaceId($item['google_place_id']);
        $details = $googlePlaces->details();

        // Error checking.
        if (!empty($details['errors'])) {
          foreach($details['errors'] as $error) {
            watchdog('gp', 'Error fetching Google Place details: @error', array('@error' => $error), WATCHDOG_ERROR);
          }
          continue;
        }
        if (!isset($details) || !isset($details['result']) || empty($details['result'])) {
          continue;
        }

        // @TODO Should this be configurable? Many of these mentioned
        // here are required, some are optional.
        $components_to_save = array(
          'address_components',
          'formatted_address',
          'geometry',
          'icon',
          'international_phone_number',
          'name',
          'types',
          'url',
          'website',
          'price_level',
          'utc_offset',
          'opening_hours',
          'photos'
        );
        foreach($details['result'] as $component => $value) {
          if (!in_array($component, $components_to_save)) {
            unset($details['result'][$component]);
          }
        }
        $place = $details['result'];

        // Set the place in the cache.
        $cache_expiration = gp_cache_expiration();
        if ($cache_expiration) {
          cache_set($place_id, $place, 'cache_gp', REQUEST_TIME + $cache_expiration);
        }
      }

      // Set the place name as the label (i.e. title) of the entity.
      $set_label = isset($instance['settings']['set_label']) ? $instance['settings']['set_label'] : TRUE;
      $info = entity_get_info($entity_type);
      if ($set_label && !empty($info['entity keys']['label'])) {
        // Replace any emojis with an empty space and trim it.
        $entity->{$info['entity keys']['label']} = trim(
          preg_replace(
            '/[\x00-\x1F\x80-\xFF]/',
            '',
            mb_convert_encoding($place['name'], "UTF-8")
          )
        );
      }

      // Addressfield
      if (isset($place['address_components']) && !empty($instance['settings']['addressfield'])) {
        $field_name = $instance['settings']['addressfield'];
        $address = gp_address_from_components($place['address_components']);
        $field_values[$field_name] = array(
          $address,
        );
      }

      // Formatted Address
      if (isset($place['formatted_address']) && !empty($instance['settings']['formatted_address'])) {
        $field_name = $instance['settings']['formatted_address'];
        $field_values[$field_name] = array(
          array('value' => $place['formatted_address'])
        );
      }

      // Geofield
      if (isset($place['geometry']) && !empty($instance['settings']['geofield'])) {
        $field_name = $instance['settings']['geofield'];
        $lat = $place['geometry']['location']['lat'];
        $lon = $place['geometry']['location']['lng'];
        $field_values[$field_name] = array(
          array(
            'geom' => "POINT ($lon $lat)",
            'geo_type' => 'point',
            'lat' => $lat . "000000",
            'lon' => $lon . "000000",
            'left' => $lon . "000000",
            'top' => $lat . "000000",
            'right' => $lon . "000000",
            'bottom' => $lat . "000000"
          )
        );
      }

      // Telephone
      if (isset($place['international_phone_number']) && !empty($instance['settings']['telephone'])) {
        $field_name = $instance['settings']['telephone'];
        $field_values[$field_name] = array(
          array('value' => $place['international_phone_number'])
        );
      }

      // Website
      if (isset($place['website']) && !empty($instance['settings']['website'])) {
        $field_name = $instance['settings']['website'];
        $field_values[$field_name] = array(
          array('value' => $place['website'])
        );
      }

      // Icon
      if (isset($place['icon']) && !empty($instance['settings']['icon'])) {
        $field_name = $instance['settings']['icon'];
        $field_values[$field_name] = array(
          array('value' => $place['icon'])
        );
      }

      // URL (Google Plus)
      if (isset($place['url']) && !empty($instance['settings']['url'])) {
        $field_name = $instance['settings']['url'];
        $field_values[$field_name] = array(
          array('value' => $place['url'])
        );
      }

      // Place Types (via taxonomy terms)
      if (isset($place['types']) && !empty($instance['settings']['types'])) {
        $field_name = $instance['settings']['types'];
        $taxonomy_field_instance = field_info_field($field_name);
        $vocabularies = taxonomy_vocabulary_get_names();
        $vocabulary_machine_name = $taxonomy_field_instance['settings']['allowed_values'][0]['vocabulary'];
        $vocabulary = $vocabularies[$vocabulary_machine_name];
        foreach ($place['types'] as $delta => $type) {
          $terms = taxonomy_get_term_by_name($type, $vocabulary_machine_name);
          $term = NULL;
          if (empty($terms)) {
            $term = new stdClass();
            $term->vid = $vocabulary->vid;
            $term->name = $type;
            taxonomy_term_save($term);
          }
          else {
            foreach ($terms as $tid => $term) {
              if ($term->name == $type) { break; }
            }
          }
          $field_values[$field_name][$delta] = array('tid' => $term->tid);
        }
      }

      // Price Level
      if (isset($place['price_level']) && !empty($instance['settings']['price_level'])) {
        $field_name = $instance['settings']['price_level'];
        $field_values[$field_name] = array(
          array('value' => $place['price_level'])
        );
      }

      // UTC Offset
      if (isset($place['utc_offset']) && !empty($instance['settings']['utc_offset'])) {
        $field_name = $instance['settings']['utc_offset'];
        $field_values[$field_name] = array(
          array('value' => $place['utc_offset'])
        );
      }

      // Weekday Text
      if (!empty($instance['settings']['weekday_text']) && !empty($place['opening_hours']['weekday_text'])) {
        $field_name = $instance['settings']['weekday_text'];
        $field_values[$field_name] = array(
          array('value' => implode(',', $place['opening_hours']['weekday_text']))
        );
      }

      // Photo(s)
      if (isset($place['photos']) && !empty($instance['settings']['photos'])) {
        $field_name = $instance['settings']['photos'];
        $photo_field_info = field_info_field($field_name);
        $photo_count = sizeof($place['photos']);
        if ($photo_field_info['cardinality'] == -1) { }
        else if ($photo_field_info['cardinality'] > $photo_count) { }
        else { $photo_count = $photo_field_info['cardinality']; }
        $field_values[$field_name] = array();
        for ($i = 0; $i < $photo_count; $i++) {
          $field_values[$field_name][$i] = array('value' => $place['photos'][$i]['photo_reference']);
        }
      }
    }
  }

  // Assign the new values to each field on the entity.
  $entity_langcode = entity_language($entity_type, $entity);
  foreach ($field_values as $field_name => $value) {
    $field = field_info_field($field_name);
    $field_lang = $langcode;
    // Because this field could be translated separately from the entity,
    // make sure to use the langcode for that field.
    if (field_is_translatable($entity_type, $field)) {
      $field_lang = !empty($entity_langcode) ? $entity_langcode : $entity->language;
    }
    $entity->{$field_name}[$field_lang] = $value;
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Display a message about the number of GP fields.
 */
function gp_form_field_ui_field_edit_form_alter(&$form, &$form_state, $form_id) {
  if (isset($form['#field']['type']) && $form['#field']['type'] == 'google_place') {
    if (!isset($form['field']['cardinality']['#prefix'])) {
      $form['field']['cardinality']['#prefix'] = '';
    }
    $form['field']['cardinality']['#prefix'] .= '<div class="messages warning">'
      . t('Google Places field types only work with a single value field.') . '</div>';
  }
}

/**
 * Implements hook_field_is_empty().
 */
function gp_field_is_empty($item, $field) {
  if (empty($item['google_place_id'])) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Implements hook_field_validate().
 */
function gp_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
  // Loop through field items in the case of multiple values.
  foreach ($items as $delta => $item) {
    if (isset($item['google_place_id']) && $item['google_place_id'] != '') {
      // @todo What should go here?
    }
  }
}

/**
 * Implements hook_field_formatter_info().
 */
function gp_field_formatter_info() {
  return array(
    'google_place_default' => array(
      'label' => t('Default'),
      'field types' => array('google_place'),
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 */
function gp_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  foreach ($items as $delta => $item) {
    if (!isset($item['google_place_id'])) {
      continue;
    }

    switch ($display['type']) {
      case 'google_place_default':
        $element[$delta]['#markup'] = $item['google_place_id'];
        break;
    }
  }
  return $element;
}

