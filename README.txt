# Google Places for Drupal


VERY IMPORTANT
==============

Before using this module, make sure you understand the terms and conditions
Google has set forth for its Places data and how you can use it:

https://developers.google.com/places/policies#pre-fetching_caching_or_storage_of_content
https://developers.google.com/maps/terms#section_10_1_3


INSTALLATION
============

1. Install the google-places library.

Visit the following link to download the library:

  https://github.com/anthony-mills/google-places/releases/tag/v1.2

Extract the file and rename the folder to the following location:

  sites/all/libraries/google-places

Once extracted, you should see the file at the following location:

  sites/all/libraries/google-places/src/mills/google-places/googlePlaces.php

2. Download the module to sites/all/modules, and enable it as usual.

3. Go to admin/config/services/gp and enter your Google Places API Key.

If you need an API key, follow the instructions here:

  https://developers.google.com/places/web-service/get-api-key


USAGE
=====

As the developer, you decide when to create/edit a node and insert a place id
into the field. Once you save the node, this module performs a call to the
Google Places API, fetches the results then saves them into the various fields
within the node.

To set this up, create or modify a content type on your site, and add a
single-value Google Place field. When setting up this field, you can specify
the other fields on this content type that will be populated with the cached
data. Some field types can be created using basic field types like Text
and Integer such as Websites, Photos, UTC Offset, and more. Below are some
examples/instructions of field types from contributed modules.

IMPORTANT: Once you have finished adding the fields to your content type, go
to the 'Manage Display' page for your content type, and hide all of these
fields from public diplay, unless you want to display them in conjunction with
a Google Map, per the terms and conditions of the Google Places API.


A) Addressfield -- https://www.drupal.org/project/addressfield

Add a non-required address field to your content type. Be sure to select
no countries so they are all available, and check the
'Address form (country-specific)' and 'Make all fields optional' checkboxes
under the 'Format handlers' settings.

B) Geofield -- https://www.drupal.org/project/geofield

Add a non-required geo field to your content type, use the
'Latitude / Longitude' widget.

C) Telephone -- https://www.drupal.org/project/telephone

Add a non-required telephone field to your content type.

D) URL -- https://www.drupal.org/project/url

Add 3 non-required url fields to your content type to hold the place's
website URL, Google Plus URL and Google Maps Icon URL.

E) Taxonomy Term

Add a non-required, unlimited value taxonomy term reference field to hold
onto the place's types. It is recommend to create a new vocabulary for this,
e.g. "Google Places Types".

F) Photo References

Add a non-required URL text field with a length of 512 to your content type
to hold onto the place photo reference(s). You can specify a single value, multi
value or unlimited value (10 max) field here to decide how many photo(s) you'd
like to save a reference to.


KNOWN ISSUES
============

Google Places fields can be added to any type of fieldable entity, including
nodes, users, taxonomy terms, and even custom entity types that are fieldable.
However, there is currently a limitation where the expiration and refetching
of Place data is only performed on nodes. This will be addressed in a future
release.
